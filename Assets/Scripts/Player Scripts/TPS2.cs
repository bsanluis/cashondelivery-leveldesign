﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPS2 : MonoBehaviour
{
    [Header("Movement Options")]
    public float speed;
    public float jumpSpeed;
    public float gravityScale;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    Vector2 rotation = Vector2.zero;

    public Transform pivot;
    public float rotateSpeed;
    public Animator anim;
    public GameObject playerModel;
    public ParticleSystem particle;

    [Header("Knockback Settings")]
    public float knockbackForce;
    public float knockbackTime;
    private float knockbackCounter;
    private float knockbackDisableMovement = 0.5f;
    private float knockbackDisableCounter = 0.0f;

    [HideInInspector] public bool canMove = true;
    [HideInInspector] public bool doubleJump = false;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // this is placed here to allow the player to move while in the air
        //moveDirection = new Vector3(Input.GetAxis("Horizontal") * speed, moveDirection.y, Input.GetAxis("Vertical") * speed);

        if (knockbackCounter < 0)
        {
            float yStore = moveDirection.y;
            moveDirection = (transform.forward * Input.GetAxisRaw("Vertical")) + (transform.right * Input.GetAxisRaw("Horizontal"));
            moveDirection = moveDirection.normalized * speed;
            moveDirection.y = yStore;

            if (characterController.isGrounded)
            {
                moveDirection.y = 0.0f;
                doubleJump = true;
                if (Input.GetButtonDown("Jump"))
                {
                    moveDirection.y = jumpSpeed;
                    anim.Play("Jump", -1, 0.0f);
                }
            }
            else
            {
                if (Input.GetButtonDown("Jump") && doubleJump)
                {
                    moveDirection.y = jumpSpeed;
                    doubleJump = false;
                    particle.Play();
                    anim.Play("Jump", -1, 0.0f);
                }
            }
        }
        else
        {
            knockbackCounter -= Time.deltaTime;
            knockbackDisableCounter -= Time.deltaTime;
            if (knockbackDisableCounter < 0 && characterController.isGrounded)
            {
                moveDirection = new Vector3(0.0f, 0.0f, 0.0f);
            }
        }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
        characterController.Move(moveDirection * Time.deltaTime);

        anim.SetBool("isGrounded", characterController.isGrounded);
        anim.SetFloat("speed", Mathf.Abs(Input.GetAxisRaw("Vertical")) + Mathf.Abs(Input.GetAxisRaw("Horizontal")));

        if ((Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0) && knockbackCounter < 0)
        {
            transform.rotation = Quaternion.Euler(0.0f, pivot.rotation.eulerAngles.y, 0.0f);
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0.0f, moveDirection.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, rotateSpeed * Time.deltaTime);
        }

    }

    public void Knockback(Vector3 direction)
    {
        knockbackCounter = knockbackTime;
        knockbackDisableCounter = knockbackDisableMovement;
        moveDirection = direction * knockbackForce;
        moveDirection.y = knockbackForce;
    }
}

// refernce : https://www.youtube.com/watch?v=p5OWasgD1no&list=PLiyfvmtjWC_V_H-VMGGAZi7n5E0gyhc37&index=3
// ledge grab script : https://www.reddit.com/r/Unity3D/comments/71kwqa/need_help_with_ledge_climbing_script/ (requires alot of prefab placements)
// flxing slope bouncing : https://www.youtube.com/watch?v=b7bmNDdYPzU