﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestHolder : MonoBehaviour
{
    private DialogueManager dialogueManager;
    private DialogueTrigger dialogueTrigger;
    private QuestMaster questMaster;
    private GameObject camera; // find main camera, but it should be automatic

    [Header("Quest Options")]
    public bool questGiver;
    public bool questFinished;

    [Header("Variable Holders")]
    public GameObject targetDelivery;

    void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        dialogueTrigger = gameObject.GetComponent<DialogueTrigger>();
        questMaster = GameObject.FindWithTag("Player").GetComponent<QuestMaster>();
        camera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown("e"))
        {
            if (dialogueManager.isOpen == false)
            {
                dialogueTrigger.TriggerDialogue();
            }
            else
            {
                dialogueManager.DisplayNextSentence();
                if (dialogueManager.sentences.Count == 0)
                {
                    // activate quest
                    // if there is no quest active, enable marker and set marker

                    // else make the marker disappear
                    if (questGiver)
                    {
                        camera.GetComponent<DestinationWaypoint>().target = targetDelivery.transform;
                        questMaster.activeQuest = true;
                    }
                    else
                    {
                        camera.GetComponent<DestinationWaypoint>().DisableMarker();
                        questMaster.activeQuest = false;
                    }
                    // give item when necessary
                }
            }
        }
    }
}

// scroll cellphone: https://www.youtube.com/watch?v=MWOvwegLDl0
// https://www.youtube.com/watch?v=lUun2xW6FJ4