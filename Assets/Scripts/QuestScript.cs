﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestScript : MonoBehaviour
{
    private DialogueManager dialogueManager;
    private DialogueTrigger dialogueTrigger;

    [Header("Quest Options")]
    public bool questGiver;

    [Header("Variable Holders")]
    public GameObject targetDelivery;
    public GameObject camera; // find main camera, but it should be automatic

    void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        dialogueTrigger = gameObject.GetComponent<DialogueTrigger>();
        camera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown("e"))
        {
            if (dialogueManager.isOpen == false)
            {
                dialogueTrigger.TriggerDialogue();
            }
            else
            {
                dialogueManager.DisplayNextSentence();
                if (dialogueManager.sentences.Count == 0)
                {
                    // activate quest
                    // if there is no quest active, enable marker and set marker

                    // else make the marker disappear
                    if (questGiver)
                    {
                        camera.GetComponent<DestinationWaypoint>().target = targetDelivery.transform;
                    }
                    else
                    {
                        camera.GetComponent<DestinationWaypoint>().DisableMarker();
                    }
                    // give item when necessary
                }
            }
        }
    }
}