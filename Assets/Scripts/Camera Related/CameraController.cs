﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public Transform playerLookAt;
    public Vector3 offset;

    public float rotateSpeed; // how fast the camera rotates
    public float minViewAngle;
    public float maxViewAngle;


    public Transform pivot;

    void Start()
    {
        offset = player.position - transform.position;

        pivot.transform.position = player.transform.position;
        //pivot.transform.parent = player.transform;
        pivot.transform.parent = null;

        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void LateUpdate()
    {
        pivot.transform.position = player.transform.position;

        float horizontal = Input.GetAxisRaw("Mouse X") * rotateSpeed;
        pivot.Rotate(0.0f, horizontal, 0.0f);

        float vertical = Input.GetAxisRaw("Mouse Y") * rotateSpeed;
        //vertical = Mathf.Clamp(vertical, -2, 2); will sort this out later
        pivot.Rotate(-vertical, 0.0f, 0.0f);

        //pivot.rotation = Quaternion.Euler(Mathf.Clamp(pivot.eulerAngles.x, 0.0f, maxViewAngle), pivot.eulerAngles.y, 0.0f);
        
        if (pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180.0f)
        {
            pivot.rotation = Quaternion.Euler(maxViewAngle, pivot.eulerAngles.y, 0.0f);
        }
        if (pivot.rotation.eulerAngles.x > 180.0f && pivot.rotation.eulerAngles.x < 360.0f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360.0f + minViewAngle, pivot.eulerAngles.y, 0.0f);
        }

        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0.0f);
        transform.position = player.position - (rotation * offset);

        //transform.position = player.position - offset;

        if (transform.position.y < player.position.y)
        {
            transform.position = new Vector3(transform.position.x, player.position.y, transform.position.z);
        }

        transform.LookAt(playerLookAt);
    }
}