﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    private Transform playerTransform;
    public bool allowRotateCamera;

    private void Awake()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
    }

    void LateUpdate()
    {
        Vector3 newPosition = playerTransform.position;
        newPosition.y = transform.position.y;
        transform.position = newPosition;

        if (allowRotateCamera)
        {
            transform.rotation = Quaternion.Euler(90.0f, playerTransform.eulerAngles.y, 0.0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
        }
    }
}

//https://www.youtube.com/watch?v=28JTTXqMvOU