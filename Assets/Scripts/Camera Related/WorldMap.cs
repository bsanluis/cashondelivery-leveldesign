﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMap : MonoBehaviour
{
    // the purpose of this script is to open the world map of the player as a toggle
    // the tabkey will be the toggle for the map
    // when the map is active, the game is not paused, but the minimap closes
    public RawImage mainMap;
    public RawImage miniMap;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("tab"))
        {
            if (!mainMap.IsActive())
            {
                mainMap.gameObject.SetActive(true);
                miniMap.gameObject.SetActive(false);
            }
            else
            {
                mainMap.gameObject.SetActive(false);
                miniMap.gameObject.SetActive(true);
            }
        }
    }
}