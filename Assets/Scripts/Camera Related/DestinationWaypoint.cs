﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DestinationWaypoint : MonoBehaviour
{
    public Image waypointMarker;
    public Transform target;
    public TextMeshProUGUI distanceText;
    public Vector3 offset;

    [Header("Scale Options")]
    public Vector3 minScale;
    public Vector3 maxScale;

    private GameObject player;

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        float minX = waypointMarker.GetPixelAdjustedRect().width / 2;
        float maxX = Screen.width - minX;

        float minY = waypointMarker.GetPixelAdjustedRect().height / 2;
        float maxY = Screen.height - minY;

        Vector2 pos = Camera.main.WorldToScreenPoint(target.position + offset);

        if (Vector3.Dot((target.position - transform.position), transform.forward) < 0)
        {
            // target is behind the player
            if (pos.x < Screen.width / 2)
            {
                pos.x = maxX;
            }
            else
            {
                pos.x = minX;
            }
        }

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        waypointMarker.transform.position = pos;
        distanceText.text = ((int)Vector3.Distance(target.position, player.transform.position)).ToString() + "m";
    }

    public void EnableMarker()
    {
        waypointMarker.gameObject.SetActive(true);
    }

    public void DisableMarker()
    {
        waypointMarker.gameObject.SetActive(false);
    }
}

// reference : https://www.youtube.com/watch?v=oBkfujKPZw8